CFLAGS=-Wall -Wextra
LDFLAGS=-lpam -lutempter -ldl

SBINDIR=/sbin
MANDIR=/usr/share/man

all: autologin

clean:
	$(RM) autologin

install: $(DESTDIR)$(SBINDIR)/autologin $(DESTDIR)$(MANDIR)/man8/autologin.8.gz

$(DESTDIR)$(SBINDIR) $(DESTDIR)$(MANDIR):
	mkdir -p $@

$(DESTDIR)$(SBINDIR)/autologin: $(DESTDIR)$(SBINDIR) autologin
	install -m 711 $^ $@

$(DESTDIR)$(MANDIR)/man8/autologin.8.gz: $(DESTDIR)$(MANDIR) autologin.8
	cat autologin.8 | gzip > $(DESTDIR)$(MANDIR)/man8/autologin.8.gz

GITTAG=autologin-$(shell rpm -q --specfile autologin.spec --qf '%{version}-%{release}\n' | head -n1)
distdir=$(shell rpm -q --specfile --qf '%{name}-%{version}-%{release}\n' autologin.spec | head -n1)
ORIGIN=$(shell git config --get remote.origin.url)

tag:
	git tag $(GITTAG)

force-tag:
	git tag -f $(GITTAG)

ARCHIVEOUTDIR=$(shell pwd)

local-archive:
	$(MAKE) archive ORIGIN=$(ARCHIVEOUTDIR)

archive:
	repo=`pwd`; \
	tmpdir=`mktemp -d /tmp/make_archive_XXXXXX`; \
	if test -d "$$tmpdir" ; then \
		git clone $(ORIGIN) $$tmpdir/$(distdir);\
		cd $$tmpdir/$(distdir);\
		git checkout $(GITTAG);\
		cd $$tmpdir;\
		tar cvz --exclude=.git -f $(distdir).tar.gz $(distdir); \
		mkdir -p $$tmpdir/rpm-build-top;\
		rpmbuild \
			--define "_topdir $$tmpdir/rpm-build-top" \
			--define "_sourcedir $$tmpdir/rpm-build-top" \
			--define "_specdir $$tmpdir/rpm-build-top" \
			--define "_builddir $$tmpdir/rpm-build-top" \
			--define "_srpmdir $$tmpdir/rpm-build-top" \
			--define "_rpmdir $$tmpdir/rpm-build-top" \
			--define "_rpmdir $$tmpdir/rpm-build-top" \
			-tb $(distdir).tar.gz;\
		cp -v $(distdir).tar.gz $(ARCHIVEOUTDIR)/;\
		rm -fr $$tmpdir;\
	fi
