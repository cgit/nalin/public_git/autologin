Summary: Auto Login Utility
Name: autologin
Version: 0.02
Release: 1
License: GPLv2
Group: Security Violations
Source: autologin-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-root

%description
This is a program you can use to automatically log in on a workstation when
it boots. This program is normally run from /etc/inittab replacing one of
your mingetty lines. This program is a huge security hole.

%prep
%setup

%build
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README TODO
%attr(711,root,root) /sbin/autologin
%{_mandir}/man8/autologin.8.gz

%changelog
* Thu Mar 20 2008 Nalin Dahyabhai <nalin@redhat.com>
- try to clean up reliance on some non-portable functions
- set PAM's environment variables in the process environment

* Thu Mar 16 2000 Dale Lovelace <dale@redhat.com>
- First RPM build
